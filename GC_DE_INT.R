#############################################################
# DE of final merged bam files of GC #
#############################################################
setwd("~/Dropbox/Brassica_DE_stuff/")

library(edgeR)
library("ggplot2")

counts_GC_final_merged <- read.delim("GC_final_merged_counts.tsv",row.names=NULL)
counts_GC_final_merged<-counts_GC_final_merged[counts_GC_final_merged$gene!="*",]
row.names(counts_GC_final_merged) <- counts_GC_final_merged$gene
counts_GC_final_merged <- counts_GC_final_merged[,-1]
head(counts_GC_final_merged)
tail(counts_GC_final_merged)
counts_GC_final_merged[is.na(counts_GC_final_merged)] <- 0
names(counts_GC_final_merged)
summary(counts_GC_final_merged)
sort(colSums(counts_GC_final_merged))
counts_GC_final_merged <- counts_GC_final_merged[,colSums(counts_GC_final_merged) > 100000]
names(counts_GC_final_merged)

#To check for clustering or not#
data_GC_final_merged <- DGEList(counts=counts_GC_final_merged,group=c(rep("IMB211", 31), rep("R500", 35)))
data_GC_final_merged$samples
dim(data_GC_final_merged)
data_GC_final_merged <- data_GC_final_merged[rowSums(cpm(data_GC_final_merged) > 1) >= 3,]
dim(data_GC_final_merged)
data_GC_final_merged <- calcNormFactors(data_GC_final_merged)
data_GC_final_merged$samples
#MDS plot to produce the coordinates for the samples that are clustered#
MDS.plot_GC_final_merged<-plotMDS(data_GC_final_merged, cex = 0.7)
#ggplot to check to see how the samples are clustered#
parsed_GC_final_merged <- matrix(unlist(strsplit(names(MDS.plot_GC_final_merged$x),"_")), ncol = 4, byrow=T)
MDS_DE_GC_final_merged <- data.frame(MDS.plot_GC_final_merged$x, MDS.plot_GC_final_merged$y, parsed_GC_final_merged)
names(MDS_DE_GC_final_merged) <- c("x", "y", "par", "treatment", "rep", "tissue")
mds_GC_final_merged <- ggplot(MDS_DE_GC_final_merged, aes(x, y)) +
  geom_point(aes(color = treatment, shape = rep)) + geom_rug(aes(colour = treatment)) +
  facet_grid(par ~ tissue) + labs(title = "ggplot of GC final merged samples")

mds_GC_final_merged # Looks good!
ggsave(file = "ggplot_GC_final_merged_samples.pdf",mds_GC_final_merged)

##################################################################
# INTERNODE #
###################################################################
# Extract the columns for Internode tissue
grep("INT", names(counts_GC_final_merged))
count_GC_INT<-counts_GC_final_merged[c(2,8,17,22,27,34,41,46,52,57,62)] 
names(count_GC_INT)

# To check to see if the samples are clustered or not? #
data_GC_INT <- DGEList(counts=count_GC_INT,group=c(rep("IMB211",5), rep("R500",6)))
data_GC_INT$samples
dim(data_GC_INT)
data_GC_INT <- data_GC_INT[rowSums(cpm(data_GC_INT) > 1) >= 3,]
dim(data_GC_INT)
data_GC_INT <- calcNormFactors(data_GC_INT)
data_GC_INT$samples
MDS_GC_INT<-plotMDS(data_GC_INT, cex=1.0)
#ggplot
parsed_GC_INT<-matrix(unlist(strsplit(names(MDS_GC_INT$x),"_")), ncol = 4, byrow=T)
head(parsed_GC_INT)
MDS_GC_INT_2 <- data.frame(MDS_GC_INT$x, MDS_GC_INT$y, parsed_GC_INT)
names(MDS_GC_INT_2) <- c("x", "y", "par", "treatment", "rep", "tissue")
mds_GC_INT <- ggplot(MDS_GC_INT_2, aes(x, y)) +
  geom_point(aes(color = par, shape = rep)) + facet_grid(par ~ .) + theme(aspect.ratio=0.5)
mds_GC_INT + labs(title = "ggplot") # There is a clear separation of the genotypes. Looks good for Internode tissue
ggsave(file = "ggplot_GC_INT_samples.pdf", mds_GC_INT)

# DE analysis #
# Can't use floating number for glmRT analysis. So need to round off the number
count_GC_INT<-round(count_GC_INT,0)
samples <- data.frame(
  file=names(count_GC_INT),
  gt=sub("(IMB211|R500)_[[:print:]]+","\\1",names(count_GC_INT)),
  trt=sub("[[:print:]]+_(SUN|SHADE)[[:print:]]+","\\1",names(count_GC_INT)),
  rep=sub("[[:alnum:]]+_[[:alnum:]]+_([1-3])[[:print:]]+.bam","\\1",names(count_GC_INT))
  )
samples
group = paste(samples$gt, samples$trt, sep = "_")
data_GC_INT.int <- DGEList(count_GC_INT,group=group)
data_GC_INT.int$samples
data_GC_INT.int <- data_GC_INT.int[rowSums(cpm(data_GC_INT.int) > 1) >= 3,]
dim(data_GC_INT.int)
data_GC_INT.int <- calcNormFactors(data_GC_INT.int)
data_GC_INT.int$samples
# relevel here
samples$trt <- relevel(samples$trt, ref="SUN")
# set-up model matrix design
design_GC_INT_int <- model.matrix(~samples$gt*samples$trt)
colnames(design_GC_INT_int)
design_GC_INT_int
rownames(design_GC_INT_int) <- colnames(data_GC_INT.int)
head(design_GC_INT_int)
# Estimating the dispersion
# First we estimate the overall dispersion for the dataset, to get an idea of the overall level of biological variability
data_GC_INT.int <- estimateGLMCommonDisp(data_GC_INT.int,design_GC_INT_int, verbose=TRUE)
# Then we estimate gene-wise dispersion estimates, allowing a possible trend with averge count size
data_GC_INT.int <- estimateGLMTrendedDisp(data_GC_INT.int,design_GC_INT_int)
data_GC_INT.int <- estimateGLMTagwiseDisp(data_GC_INT.int,design_GC_INT_int)
plotBCV(data_GC_INT.int, main = "comparision of common vs Trend dispersion")

# Differential expression:
# Fit the linear model to the design matrix
fit_GC_INT_int <- glmFit(data_GC_INT.int,design_GC_INT_int)
colnames(fit_GC_INT_int$design) # basically indicates coeffients
head(fit_GC_INT_int)

#########
# Testing for DE bewteen for the treatment (Sun vs Shade). For this drop all coefficients 3 

GC_INT_trt_only.lrt <- glmLRT(fit_GC_INT_int,coef=3)
names(GC_INT_trt_only.lrt)
results_GC_INT_trt_only.lrt <- topTags(GC_INT_trt_only.lrt,n=Inf)
head(results_GC_INT_trt_only.lrt$table)
sum(results_GC_INT_trt_only.lrt$table$FDR<0.01) #332
summary(de.trt1<-decideTestsDGE(GC_INT_trt_only.lrt, p.value=.01))
#    [,1] 
# -1   191
# 0  30562
# 1    141

# save results in a file:
write.csv(results_GC_INT_trt_only.lrt$table, "edgeR_DE_GC_IMB211_R500_INT_trt_only")

## Go Enrichment ##
library(Biostrings)
library(GO.db)
library(goseq)

#[upendra_35@vm142-20 Brassica_cdna_fasta_ref]$ perl ~/upendra_shared/other_scripts/get_gene_lengths.pl Brassica_rapa_final_transcriptome.fasta > Brapa_final_transcriptome_gene_lengths #only have to do it once

Slylen<- read.csv ("Brapa_final_transcriptome_gene_lengths", sep="\t", h=F)
colnames(Slylen)<-c("Gene","length")
head(Slylen)
Slylenf <- Slylen[,1]
head(Slylenf)
Slylen2 <- Slylen[,2]
head(Slylen2)
genes.in.annot<-Slylenf
head(genes.in.annot)

go <- read.table("Bra.Go_final.txt", h=F)
head(go)
colnames(go) <- c("Gene", "GO")
dim(go)
go.list <- strsplit(as.character(go[,2]),split=",",fixed=T)
head(go.list)
names(go.list) <- as.character(go[,1])
head(go.list)
length(go.list)


GC_DE_IMB211_R500_trt_only_geneExpFile <- c("edgeR_DE_GC_IMB211_R500_INT_trt_only")
GC_IMB211_R500_trt_only_DE_genes<-read.csv(GC_DE_IMB211_R500_trt_only_geneExpFile, sep = ",", quote="\"", h=T)
GC_IMB211_R500_trt_only_DE_genes<-subset(GC_IMB211_R500_trt_only_DE_genes, FDR<0.01)
head(GC_IMB211_R500_trt_only_DE_genes)
dim(GC_IMB211_R500_trt_only_DE_genes)
GC_IMB211_R500_trt_only_DE_genes<-as.character(GC_IMB211_R500_trt_only_DE_genes[,1])
GC_IMB211_R500_trt_only_not_DE_genes<-as.character(genes.in.annot[ !(genes.in.annot %in% GC_IMB211_R500_trt_only_DE_genes)])
head(GC_IMB211_R500_trt_only_not_DE_genes)
length(genes.in.annot) # 49898
length(GC_IMB211_R500_trt_only_not_DE_genes) # 49566
length(GC_IMB211_R500_trt_only_DE_genes) #332

GC_IMB211_R500_trt_only_gene_name<-c(GC_IMB211_R500_trt_only_DE_genes,GC_IMB211_R500_trt_only_not_DE_genes)
head(GC_IMB211_R500_trt_only_gene_name)
tail(GC_IMB211_R500_trt_only_gene_name)
length(GC_IMB211_R500_trt_only_gene_name)

GC_INT_trt_only_genes<-rep(c(1,0),c(length(GC_IMB211_R500_trt_only_DE_genes), length(GC_IMB211_R500_trt_only_not_DE_genes)))
head(GC_INT_trt_only_genes)
tail(GC_INT_trt_only_genes)
names(GC_INT_trt_only_genes)<-GC_IMB211_R500_trt_only_gene_name

GC_INT_trt_only_bias<-Slylen2
head(GC_INT_trt_only_bias)
names(GC_INT_trt_only_bias) <- genes.in.annot
GC_INT_trt_only_bias <- GC_INT_trt_only_bias[names(GC_INT_trt_only_bias)%in%names(GC_INT_trt_only_genes)]
head(GC_INT_trt_only_bias)

pwf_trt_only <- nullp(GC_INT_trt_only_genes,genome=NULL,id=NULL,bias.data=GC_INT_trt_only_bias)
head(pwf_trt_only)
go.analysis_trt_only <- goseq(pwf_trt_only,gene2cat=go.list)
head(go.analysis_trt_only)
nrow(go.analysis_trt_only) #1664

GC_IMB211_R500_INT_trt_only.enriched.GO_category <-go.analysis_trt_only$category[go.analysis_trt_only$over_represented_pvalue < 0.01]
head(GC_IMB211_R500_INT_trt_only.enriched.GO_category)
GC_IMB211_R500_INT_trt_only.enriched.GO_p_value <-go.analysis_trt_only$over_represented_pvalue[go.analysis_trt_only$over_represented_pvalue < 0.01]
head(GC_IMB211_R500_INT_trt_only.enriched.GO_p_value)
GC_IMB211_R500_INT_trt_only.enriched.GO<-cbind(GC_IMB211_R500_INT_trt_only.enriched.GO_category, GC_IMB211_R500_INT_trt_only.enriched.GO_p_value)
head(GC_IMB211_R500_INT_trt_only.enriched.GO)
length(GC_IMB211_R500_INT_trt_only.enriched.GO) #50
write.table(GC_IMB211_R500_INT_trt_only.enriched.GO, "GC_IMB211_R500_INT_trt_only.enriched.GO_pvalue.csv", quote =F, row.names=F)

# To put the results in a dataframe:

go.id   <- 0
go.term <- 0
go.ont  <- 0
i <- 1
for (go in GC_IMB211_R500_INT_trt_only.enriched.GO) {
  go.id[i]   <- GOID(GOTERM[[go]])
  go.term[i] <- Term(GOTERM[[go]])
  go.ont[i]  <- Ontology(GOTERM[[go]])
  i <- i + 1
}
GC_IMB211_R500_INT_trt_only.enriched.GO.df <- data.frame( go.id, go.term, go.ont)

write.csv(GC_IMB211_R500_INT_trt_only.enriched.GO.df, "GC_IMB211_R500_INT_trt_only.enriched_GO_terms.csv")


#####################
# To test for DE for gt and gt*trt effect drop coefficeint 2 and 4...
GC_INT_gt_int_lrt <- glmLRT(fit_GC_INT_int,coef=c(2,4))
names(GC_INT_gt_int_lrt)
topTags(GC_INT_gt_int_lrt) # shows what coefficient/s we are working with...
results_GC_INT_gt_int_lrt<-topTags(GC_INT_gt_int_lrt, n=Inf)
head(results_GC_INT_gt_int_lrt$table) 
sum(results_GC_INT_gt_int_lrt$table$FDR<0.01) # 11836 

# save results in a file:
write.csv(results_GC_INT_gt_int_lrt, "edgeR_DE_GC_IMB211_R500_INT_gt_int")

########## GO Enrichment ###################

GC_DE_IMB211_R500_INT_gt_int_geneExpFile <- c("edgeR_DE_GC_IMB211_R500_INT_gt_int")
head(GC_DE_IMB211_R500_INT_gt_int_geneExpFile)
GC_IMB211_R500_INT_gt_int_DE_genes<-read.csv(GC_DE_IMB211_R500_INT_gt_int_geneExpFile, sep = ",", quote="\"", h=T)
GC_IMB211_R500_INT_gt_int_DE_genes<-subset(GC_IMB211_R500_INT_gt_int_DE_genes, FDR<0.01)
head(GC_IMB211_R500_INT_gt_int_DE_genes)
dim(GC_IMB211_R500_INT_gt_int_DE_genes)
GC_IMB211_R500_INT_gt_int_DE_genes<-as.character(GC_IMB211_R500_INT_gt_int_DE_genes[,1])
GC_IMB211_R500_INT_gt_int_not_DE_genes<-as.character(genes.in.annot[ !(genes.in.annot %in% GC_IMB211_R500_INT_gt_int_DE_genes)])
head(GC_IMB211_R500_INT_gt_int_not_DE_genes)
length(genes.in.annot) # 49898
length(GC_IMB211_R500_INT_gt_int_not_DE_genes) # 38062
length(GC_IMB211_R500_INT_gt_int_DE_genes) #11836

GC_IMB211_R500_INT_gt_int_gene_name<-c(GC_IMB211_R500_INT_gt_int_DE_genes,GC_IMB211_R500_INT_gt_int_not_DE_genes)
head(GC_IMB211_R500_INT_gt_int_gene_name)
tail(GC_IMB211_R500_INT_gt_int_gene_name)
length(GC_IMB211_R500_INT_gt_int_gene_name)

GC_INT_gt_int_genes<-rep(c(1,0),c(length(GC_IMB211_R500_INT_gt_int_DE_genes), length(GC_IMB211_R500_INT_gt_int_not_DE_genes)))
head(GC_INT_gt_int_genes)
tail(GC_INT_gt_int_genes)
names(GC_INT_gt_int_genes)<-GC_IMB211_R500_INT_gt_int_gene_name

GC_INT_gt_int_bias<-Slylen2
head(GC_INT_gt_int_bias)
names(GC_INT_gt_int_bias) <- genes.in.annot
GC_INT_gt_int_bias <- GC_INT_gt_int_bias[names(GC_INT_gt_int_bias)%in%names(GC_INT_gt_int_genes)]
head(GC_INT_gt_int_bias)

pwf_INT_gt_int <- nullp(GC_INT_gt_int_genes,bias.data=GC_INT_gt_int_bias)
head(pwf_INT_gt_int)
go_INT_gt_int_analysis <- goseq(pwf_INT_gt_int,gene2cat=go.list)
head(go_INT_gt_int_analysis)
nrow(go_INT_gt_int_analysis) #1664

GC_IMB211_R500_INT_gt_int.enriched.GO_category <-go_INT_gt_int_analysis$category[go_INT_gt_int_analysis$over_represented_pvalue < 0.01]
head(GC_IMB211_R500_INT_gt_int.enriched.GO_category)
GC_IMB211_R500_INT_gt_int.enriched.GO_p_value <-go_INT_gt_int_analysis$over_represented_pvalue[go_INT_gt_int_analysis$over_represented_pvalue < 0.01]
head(GC_IMB211_R500_INT_gt_int.enriched.GO_p_value)
GC_IMB211_R500_INT_gt_int.enriched.GO<-cbind(GC_IMB211_R500_INT_gt_int.enriched.GO_category, GC_IMB211_R500_INT_gt_int.enriched.GO_p_value)
head(GC_IMB211_R500_INT_gt_int.enriched.GO)
length(GC_IMB211_R500_INT_gt_int.enriched.GO) #202
write.table(GC_IMB211_R500_INT_gt_int.enriched.GO, "GC_IMB211_R500_INT_gt_int.enriched.GO_pvalue.csv", quote =F, row.names=F)

# To put the results in a dataframe:

go.id   <- 0
go.term <- 0
go.ont  <- 0
i <- 1
for (go in GC_IMB211_R500_INT_gt_int.enriched.GO) {
  go.id[i]   <- GOID(GOTERM[[go]])
  go.term[i] <- Term(GOTERM[[go]])
  go.ont[i]  <- Ontology(GOTERM[[go]])
  i <- i + 1
}
GC_IMB211_R500_INT_gt_int.enriched.GO.df <- data.frame( go.id, go.term, go.ont)

write.csv(GC_IMB211_R500_INT_gt_int.enriched.GO.df , "GC_IMB211_R500_INT_gt_int.enriched_GO_terms.csv")


##############################################
#  To find genes that respond differently in IMB211 vs R500 (gt), you should drop gt (coef=2) only
GC_INT_gt_only.lrt <- glmLRT(fit_GC_INT_int,coef=2)
names(GC_INT_gt_only.lrt)
results_GC_INT_gt_only.lrt <- topTags(GC_INT_gt_only.lrt,n=Inf) # without n=Inf gives you the coefficient
head(results_GC_INT_gt_only.lrt$table) 
sum(results_GC_INT_gt_only.lrt$table$FDR<=0.01) # 9817

# save results in a file:
write.csv(results_GC_INT_gt_only.lrt, "edgeR_DE_GC_IMB211_R500_INT_gt_only")

######## Go Enrichment ######
GC_DE_IMB211_R500_INT_gt_only_geneExpFile <- c("edgeR_DE_GC_IMB211_R500_INT_gt_only")
head(GC_DE_IMB211_R500_INT_gt_only_geneExpFile)
GC_IMB211_R500_INT_gt_only_DE_genes<-read.csv(GC_DE_IMB211_R500_INT_gt_only_geneExpFile, sep = ",", quote="\"", h=T)
GC_IMB211_R500_INT_gt_only_DE_genes<-subset(GC_IMB211_R500_INT_gt_only_DE_genes, FDR<0.01)
head(GC_IMB211_R500_INT_gt_only_DE_genes)
dim(GC_IMB211_R500_INT_gt_only_DE_genes)
GC_IMB211_R500_INT_gt_only_DE_genes<-as.character(GC_IMB211_R500_INT_gt_only_DE_genes[,1])
GC_IMB211_R500_INT_gt_only_not_DE_genes<-as.character(genes.in.annot[ !(genes.in.annot %in% GC_IMB211_R500_INT_gt_only_DE_genes)])
head(GC_IMB211_R500_INT_gt_only_not_DE_genes)
length(genes.in.annot) # 49898
length(GC_IMB211_R500_INT_gt_only_not_DE_genes) # 40081
length(GC_IMB211_R500_INT_gt_only_DE_genes)  # 9817

GC_IMB211_R500_INT_gt_only_gene_name<-c(GC_IMB211_R500_INT_gt_only_DE_genes,GC_IMB211_R500_INT_gt_only_not_DE_genes)
head(GC_IMB211_R500_INT_gt_only_gene_name)
tail(GC_IMB211_R500_INT_gt_only_gene_name)
length(GC_IMB211_R500_INT_gt_only_gene_name)

GC_INT_gt_only_genes<-rep(c(1,0),c(length(GC_IMB211_R500_INT_gt_only_DE_genes), length(GC_IMB211_R500_INT_gt_only_not_DE_genes)))
head(GC_INT_gt_only_genes)
tail(GC_INT_gt_only_genes)
names(GC_INT_gt_only_genes)<-GC_IMB211_R500_INT_gt_only_gene_name

GC_INT_gt_only_bias<-Slylen2
head(GC_INT_gt_only_bias)
names(GC_INT_gt_only_bias) <- genes.in.annot
GC_INT_gt_only_bias <- GC_INT_gt_only_bias[names(GC_INT_gt_only_bias)%in%names(GC_INT_gt_only_genes)]
head(GC_INT_gt_only_bias)

pwf_INT_gt_only <- nullp(GC_INT_gt_only_genes,bias.data=GC_INT_gt_only_bias)
head(pwf_INT_gt_only)
go_INT_gt_only_analysis <- goseq(pwf_INT_gt_only,gene2cat=go.list)
head(go_INT_gt_only_analysis)
nrow(go_INT_gt_only_analysis) #1664

GC_IMB211_R500_INT_gt_only.enriched.GO_category <-go_INT_gt_only_analysis$category[go_INT_gt_only_analysis$over_represented_pvalue < 0.01]
head(GC_IMB211_R500_INT_gt_only.enriched.GO_category)
GC_IMB211_R500_INT_gt_only.enriched.GO_p_value <-go_INT_gt_only_analysis$over_represented_pvalue[go_INT_gt_only_analysis$over_represented_pvalue < 0.01]
head(GC_IMB211_R500_INT_gt_only.enriched.GO_p_value)
GC_IMB211_R500_INT_gt_only.enriched.GO<-cbind(GC_IMB211_R500_INT_gt_only.enriched.GO_category, GC_IMB211_R500_INT_gt_only.enriched.GO_p_value)
head(GC_IMB211_R500_INT_gt_only.enriched.GO)
length(GC_IMB211_R500_INT_gt_only.enriched.GO) #168
write.table(GC_IMB211_R500_INT_gt_only.enriched.GO, "GC_IMB211_R500_INT_gt_only.enriched.GO_pvalue.csv", quote =F, row.names=F)

# To put the results in a dataframe:

go.id   <- 0
go.term <- 0
go.ont  <- 0
i <- 1
for (go in GC_IMB211_R500_INT_gt_only.enriched.GO) {
  go.id[i]   <- GOID(GOTERM[[go]])
  go.term[i] <- Term(GOTERM[[go]])
  go.ont[i]  <- Ontology(GOTERM[[go]])
  i <- i + 1
}
GC_IMB211_R500_INT_gt_only.enriched.GO.df <- data.frame( go.id, go.term, go.ont)

write.csv(GC_IMB211_R500_INT_gt_only.enriched.GO.df , "GC_IMB211_R500_INT_gt_only.enriched_GO_terms.csv")






