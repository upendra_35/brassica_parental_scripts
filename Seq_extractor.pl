#!/usr/bin/env perl
#Date:
#User:
#Use:
use strict;
use warnings;

my $extracted_fasta = "extracted.fa";
my $range = 150;
open my $positions, "<", "positions.txt";
while (<$positions>) {
    chomp;
    # my ( $seqName, $begin, $end ) = split /\s/;
    my ( $seqName, $snp_pos ) = split /\s/;
    my $begin = $snp_pos - $range;
    my $end = $snp_pos + $range;
    my $sam_cmd = "samtools ../../../db/Brapa_sequence_v1.2.fa reference.fa $seqName:$begin-$end >> $extracted_fasta";
    system($sam_cmd);
}
close($positions);





























